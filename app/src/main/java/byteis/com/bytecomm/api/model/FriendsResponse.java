package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FriendsResponse extends BaseResponse {

    private List<FriendData> data;

    public List<FriendData> getData() {
        return data;
    }

    public void setData(List<FriendData> data) {
        this.data = data;
    }
}
