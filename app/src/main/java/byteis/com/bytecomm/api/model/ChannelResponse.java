package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChannelResponse extends BaseResponse {

    @SerializedName("channels")
    private List<ChannelData> data;

    public List<ChannelData> getData() {
        return data;
    }

    public void setData(List<ChannelData> data) {
        this.data = data;
    }

}
