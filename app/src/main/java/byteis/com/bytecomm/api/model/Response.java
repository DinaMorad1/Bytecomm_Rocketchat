package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

public class Response<T> extends BaseResponse {

    @SerializedName("data")
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
