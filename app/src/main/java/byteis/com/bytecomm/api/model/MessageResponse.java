package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageResponse extends BaseResponse {

    @SerializedName("messages")
    private List<MessageData> data;

    public List<MessageData> getData() {
        return data;
    }

    public void setData(List<MessageData> data) {
        this.data = data;
    }
}
