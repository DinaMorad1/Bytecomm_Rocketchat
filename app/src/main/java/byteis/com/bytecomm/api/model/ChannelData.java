package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

public class ChannelData {

    @SerializedName("_id")
    private String serverId;

    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

}
