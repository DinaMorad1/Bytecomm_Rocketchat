package byteis.com.bytecomm.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.List;

import byteis.com.bytecomm.api.model.ChannelResponse;
import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.api.model.MessageResponse;
import byteis.com.bytecomm.api.model.Response;
import byteis.com.bytecomm.api.model.UserData;
import byteis.com.bytecomm.data.SharedPref;
import byteis.com.bytecomm.ui.contact.ChannelsContract;
import byteis.com.bytecomm.ui.contact.LoginViewModelContract;
import byteis.com.bytecomm.ui.contact.MessagesContract;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RetrofitHelper {

    private static RetrofitHelper instance;
    private RestService restService;
    private String authToken, userId;
    public final static String SOMETHING_WRONG = "Something went wrong";

    private RetrofitHelper(Context context) {
        this.authToken = SharedPref.loadString(context, SharedPref.AUTH_TOKEN);
        this.userId = SharedPref.loadString(context, SharedPref.USER_ID);
        getRestService();
    }

    public static RetrofitHelper getInstance(Context context) {
        if (instance == null)
            instance = new RetrofitHelper(context);
        return instance;
    }

    private Retrofit getClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT,
                        Modifier.STATIC)
                .create();
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(RestConstants.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit;
    }

    private void getRestService() {
        if (restService == null)
            restService = getClient().create(RestService.class);
    }

    public void getChannels(final ChannelsContract.ServerChannelsListener listener) {
        restService.getChannels(authToken, userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ChannelResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        //ToDo to be moved to common method
                        if (e == null || e.getMessage() == null || e.getMessage().isEmpty())
                            listener.onError(SOMETHING_WRONG);
                        else
                            listener.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ChannelResponse channelResponse) {
                        listener.onGetChannelsSuccess(channelResponse.getData());
                    }
                });

    }

    public void getChannelMessage(String selectedRoomId, final MessagesContract.ServerMessagesListener serverMessagesListener){
        restService.getChannelMessages(authToken, userId, selectedRoomId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MessageResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e == null || e.getMessage() == null || e.getMessage().isEmpty())
                            serverMessagesListener.onError(SOMETHING_WRONG);
                        else
                            serverMessagesListener.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(MessageResponse messageResponse) {
                        if (serverMessagesListener != null)
                            serverMessagesListener.onGetMessagesSuccess(messageResponse.getData());
                    }
                });
    }

    public void userLogin(String userNameEmail, String password, final LoginViewModelContract.ServerLoginListener loginListener) {
        rx.Observable<Response<UserData>> serverResponse;
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(userNameEmail).matches())
            serverResponse = restService.userEmailLogin(userNameEmail, password);
        else
            serverResponse = restService.userNameLogin(userNameEmail, password);

        serverResponse
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<UserData>>() {
                    @Override
                    public void onCompleted() {
                        Log.i("", "oncompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (loginListener != null) {
                            if (e == null || e.getMessage() == null || e.getMessage().isEmpty())
                                loginListener.loginError(SOMETHING_WRONG);
                            else
                                loginListener.loginError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(Response<UserData> userResponse) {
                        if (userResponse != null && userResponse.getStatus() != null) {

                            if (loginListener != null) {
                                if (userResponse.getStatus().equalsIgnoreCase(RestConstants.STATUS_SUCCESS)) {
                                    loginListener.loginSuccess(userResponse.getData());
                                } else
                                    loginListener.loginError("");
                            }
                        }
                    }
                });
    }

}
