package byteis.com.bytecomm.api;

import java.util.List;

import byteis.com.bytecomm.api.model.ChannelResponse;
import byteis.com.bytecomm.api.model.FriendData;
import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.api.model.MessageResponse;
import byteis.com.bytecomm.api.model.Response;
import byteis.com.bytecomm.api.model.UserData;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface RestService {

    @FormUrlEncoded
    @POST("login")
    Observable<Response<UserData>> userEmailLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Observable<Response<UserData>> userNameLogin(@Field("username") String username, @Field("password") String password);

    @GET("channels.list.joined")
    Observable<ChannelResponse> getChannels(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId);

    @GET("users.list")
    Observable<List<FriendData>> getFriends(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId);

    @GET("channels.history")
    Observable<MessageResponse> getChannelMessages(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId, @Query("roomId") String roomId);
}
