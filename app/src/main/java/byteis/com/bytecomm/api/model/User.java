package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("username")
    private String email;

    @SerializedName("password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
