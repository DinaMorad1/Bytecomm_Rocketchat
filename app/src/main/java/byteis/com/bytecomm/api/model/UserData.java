package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("authToken")
    private String authToken;

    @SerializedName("userId")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
