package byteis.com.bytecomm.api.model;

import com.google.gson.annotations.SerializedName;

public class MessageData {

    @SerializedName("_id")
    private String severId;

    @SerializedName("msg")
    private String messageText;

    @SerializedName("u")
    private User sender;

    @SerializedName("ts")
    private String messageDate;

    @SerializedName("groupable")
    private boolean groupable;

    public boolean isGroupable() {
        return groupable;
    }

    public void setGroupable(boolean groupable) {
        this.groupable = groupable;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getSeverId() {
        return severId;
    }

    public void setSeverId(String severId) {
        this.severId = severId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public static class User{
        @SerializedName("_id")
        private String serverId;

        @SerializedName("username")
        private String username;

        public String getServerId() {
            return serverId;
        }

        public void setServerId(String serverId) {
            this.serverId = serverId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

    }
}
