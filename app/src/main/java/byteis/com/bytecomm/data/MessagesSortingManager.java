package byteis.com.bytecomm.data;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.ui.helpers.DateFormatterHelper;

public class MessagesSortingManager {

    public static List<MessageData> sortByDate(List<MessageData> messageList) {
        Comparator<MessageData> comparator = new Comparator<MessageData>() {
            @Override
            public int compare(MessageData message1, MessageData message2) {
                long message1Long = DateFormatterHelper.getDateLong(message1.getMessageDate());
                long message2Long =  DateFormatterHelper.getDateLong(message2.getMessageDate());

                if (message1Long > message2Long)
                    return 1;
                else
                    return -1;
            }
        };

        Collections.sort(messageList, comparator);
        return messageList;
    }

}
