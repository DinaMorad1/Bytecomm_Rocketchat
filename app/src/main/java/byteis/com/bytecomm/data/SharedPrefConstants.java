package byteis.com.bytecomm.data;

public interface SharedPrefConstants {

    String AUTH_TOKEN = "authToken";

    String USER_ID = "userId";

}
