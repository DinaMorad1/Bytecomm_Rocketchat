package byteis.com.bytecomm.ui.adapters;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.List;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.model.ChannelData;
import byteis.com.bytecomm.databinding.LayoutChannelBinding;
import byteis.com.bytecomm.databinding.LayoutChannelHeaderBinding;
import byteis.com.bytecomm.model.Header;
import byteis.com.bytecomm.ui.contact.MessagesContract;
import byteis.com.bytecomm.ui.holders.ChannelViewHolder;
import byteis.com.bytecomm.ui.holders.HeaderViewHolder;

public class ChannelsAdapter extends ExpandableRecyclerAdapter<Header, ChannelData, HeaderViewHolder, ChannelViewHolder> {//extends ExpandableRecyclerAdapter<Header, ChannelData, ChannelsHeaderViewHolder, byteis.com.bytecomm.ui.holders.ChannelViewHolder> {

    private static final int HEADER_NORMAL = 0;
    private static final int CHANNEL_NORMAL = 1;
    private List<Header> headers;
    private int selectedItemPosition, selectedParentPosition;
    private Context mContext;
    //    private HashMap<Integer, ArrayList<Integer>> channelsHashMap;
    public final static String DIRECT_MESSAGES = "Direct Messages", PRIVATE_CHANNELS = "Private Channels", PUBLIC_CHANNELS = "Channels", NEARBY_CHANNELS = "Nearby", STARRED_MESSAGES = "Starred";
    private MessagesContract.ChannelSelectedListener channelSelectedListener;

    private LayoutInflater mInflater;

    public ChannelsAdapter(Context context, @NonNull List<Header> parentList, int selectedChannelPosition, int selectedParentPosition, MessagesContract.ChannelSelectedListener channelSelectedListener) {
        super(parentList);
        mInflater = LayoutInflater.from(context);
        this.headers = parentList;
        this.selectedItemPosition = selectedChannelPosition;
        this.selectedParentPosition = selectedParentPosition;
        this.mContext = context;
        this.channelSelectedListener = channelSelectedListener;
    }

    @NonNull
    @Override
    public HeaderViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        LayoutChannelHeaderBinding headerBinding = DataBindingUtil.inflate(mInflater, R.layout.layout_channel_header, parentViewGroup, false);
        return new HeaderViewHolder(headerBinding);
    }

    @NonNull
    @Override
    public ChannelViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        LayoutChannelBinding channelBinding = DataBindingUtil.inflate(mInflater, R.layout.layout_channel, childViewGroup, false);
        return new ChannelViewHolder(channelBinding);
    }

    @Override
    public void onBindParentViewHolder(@NonNull HeaderViewHolder parentViewHolder, int parentPosition, @NonNull Header header) {
        parentViewHolder.bind(header);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChannelViewHolder childViewHolder, final int parentPosition, final int childPosition, @NonNull final ChannelData channelData) {
        childViewHolder.bind(channelData);

        if (childPosition == this.selectedItemPosition && selectedParentPosition == parentPosition) {
            childViewHolder.binding.getRoot().setBackgroundColor(ActivityCompat.getColor(mContext, R.color.light_green_color));
        } else {
            childViewHolder.binding.getRoot().setBackgroundColor(ActivityCompat.getColor(mContext, R.color.light_grey));
        }

        childViewHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                channelSelectedListener.onChannelSelected(channelData.getServerId(), channelData.getName());
                setSelectedChannel(parentPosition, childPosition);
            }
        });
    }

    public void setSelectedChannel(int selectedHeaderPosition, int selectedChannelPosition) {
        this.selectedParentPosition = selectedHeaderPosition;
        this.selectedItemPosition = selectedChannelPosition;
        notifyDataSetChanged();
    }
}
