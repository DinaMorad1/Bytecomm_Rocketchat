package byteis.com.bytecomm.ui.viewmodel;

import android.content.Context;

import java.util.List;

import byteis.com.bytecomm.api.RetrofitHelper;
import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.ui.contact.MessagesContract;

public class MessagesViewModel implements MessagesContract.ServerMessagesListener {

    private Context mContext;
    private MessagesContract.MessagesView messagesView;

    public MessagesViewModel(Context mContext, MessagesContract.MessagesView messagesView) {
        this.mContext = mContext;
        this.messagesView = messagesView;
    }

    public void onChannelSelected(String roomServerId) {
        RetrofitHelper.getInstance(mContext).getChannelMessage(roomServerId, this);
    }

    @Override
    public void onGetMessagesSuccess(List<MessageData> messageDataList) {
        messagesView.onGetMessagesSuccess(messageDataList);
    }

    @Override
    public void onError(String errorMessage) {
        messagesView.onError(errorMessage);
    }
}
