package byteis.com.bytecomm.ui.fragment;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.data.MessagesSortingManager;
import byteis.com.bytecomm.databinding.FragmentMessagesBinding;
import byteis.com.bytecomm.ui.adapters.MessagesAdapter;
import byteis.com.bytecomm.ui.contact.MessagesContract;
import byteis.com.bytecomm.ui.helpers.SnackbarHelper;
import byteis.com.bytecomm.ui.viewmodel.MessagesViewModel;

public class MessagesFragment extends Fragment implements MessagesContract.MessagesView {

    private FragmentMessagesBinding binding;
    private MessagesViewModel messagesViewModel;
    private MessagesAdapter messagesAdapter;

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_messages, container, false);
        this.messagesViewModel = new MessagesViewModel(getActivity(), this);
        this.messagesAdapter = new MessagesAdapter(new ArrayList(), getActivity());
        binding.recyclerviewMessages.setAdapter(messagesAdapter);
        binding.swipeContainer.setEnabled(false);
        binding.swipeContainer.setRefreshing(true);
        return binding.getRoot();
    }

    public void setUp(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;

        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, binding.toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                binding.toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }


    @Override
    public void onChannelSelected(String roomServerId, String roomTitle) {
        messagesViewModel.onChannelSelected(roomServerId);
        binding.toolbar.setTitle(roomTitle);
        binding.swipeContainer.setRefreshing(true);
    }


    @Override
    public void onGetMessagesSuccess(List<MessageData> messageDataList) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            List<MessageData> messagesList = MessagesSortingManager.sortByDate(messageDataList);
            messagesAdapter.setItems(messagesList);
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onError(String errorMessage) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            binding.swipeContainer.setRefreshing(false);
            SnackbarHelper.showSnackbar(errorMessage, binding.getRoot(), getActivity());
        }
    }
}
