package byteis.com.bytecomm.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.model.MessageData;
import byteis.com.bytecomm.databinding.MessageCellBinding;
import byteis.com.bytecomm.log.RCLog;
import byteis.com.bytecomm.ui.helpers.DateFormatterHelper;

public class MessagesAdapter extends BaseAdapter {

    private Context context;

    public MessagesAdapter(List items, Context context) {
        super(items);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_EMPTY) {
            return super.onCreateViewHolder(parent, viewType);
        }

        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());

        final MessageCellBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.message_cell, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        int type = getItemViewType(position);

        if (type == TYPE_EMPTY)
            return;

        ViewHolder holder = (ViewHolder) viewHolder;
        MessageData messageData = (MessageData) items.get(position);
        holder.binding.messageTextView.setText(messageData.getMessageText());

        if (messageData.getMessageDate() != null && !messageData.getMessageDate().isEmpty()) {
//            Date date = DateFormatterHelper.getDate(messageData.getMessageDate());
            String dateStr = DateFormatterHelper.getFormattedDate(DateFormatterHelper.getDateLong(messageData.getMessageDate()));//date.getTime());
            holder.binding.messageDateTextView.setText(dateStr);
        }

        if (messageData.getSender() != null) {
            holder.binding.messageSenderTextView.setText(messageData.getSender().getUsername());
//            String userImgUrl = RestConstants.BASE_URL + "avatar/" + messageData.getSender().getUsername();
//            GlideHelper.loadUrl(context, userImgUrl, holder.binding.userImage);
            Picasso.with(context)
                    .load(getImageUrl(messageData.getSender().getUsername()))
                    .into(holder.binding.userImage);
        }
    }

    private String getImageUrl(String username) {
        try {
            String url = "https://" + "188.166.11.40" + "/avatar/" + URLEncoder.encode(username, "UTF-8") + ".jpg";
            return url;
        } catch (UnsupportedEncodingException exception) {
            RCLog.e(exception, "failed to get URL for user: %s", username);
            return null;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final MessageCellBinding binding;

        ViewHolder(final MessageCellBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
