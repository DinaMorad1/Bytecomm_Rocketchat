package byteis.com.bytecomm.ui.holders;


import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.animation.RotateAnimation;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;

import byteis.com.bytecomm.databinding.LayoutChannelHeaderBinding;
import byteis.com.bytecomm.model.Header;

public class HeaderViewHolder extends ParentViewHolder {

    public LayoutChannelHeaderBinding binding;

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;


    public HeaderViewHolder(LayoutChannelHeaderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(@NonNull Header header){
        binding.title.setText(header.getHeaderText());
    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                binding.arrowExpandImageview.setRotation(ROTATED_POSITION);
            } else {
                binding.arrowExpandImageview.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            binding.arrowExpandImageview.startAnimation(rotateAnimation);
        }
    }
}
