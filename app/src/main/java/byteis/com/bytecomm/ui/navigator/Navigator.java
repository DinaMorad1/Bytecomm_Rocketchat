package byteis.com.bytecomm.ui.navigator;

import android.content.Context;

public interface Navigator {

    void navigateChatActivity(Context context);

    void navigateLoginActivity(Context context);

}
