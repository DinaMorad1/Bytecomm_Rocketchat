package byteis.com.bytecomm.ui.fragment;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.model.ChannelData;
import byteis.com.bytecomm.databinding.FragmentChannelsBinding;
import byteis.com.bytecomm.model.Header;
import byteis.com.bytecomm.ui.adapters.ChannelsAdapter;
import byteis.com.bytecomm.ui.contact.ChannelsContract;
import byteis.com.bytecomm.ui.contact.MessagesContract;
import byteis.com.bytecomm.ui.viewmodel.ChannelsViewModel;

public class ChannelsFragment extends Fragment implements ChannelsContract.ChannelsView {

    private FragmentChannelsBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_channels, container, false);
        binding.setChannelsvm(new ChannelsViewModel(getActivity(), this));
        return binding.getRoot();
    }


    @Override
    public void loadChannels(List<ChannelData> channelDataList) {
        Log.i("", "" + channelDataList.size());
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Channels", channelDataList));
        binding.recyclerviewChannels.setAdapter(new ChannelsAdapter(getActivity(), headers, 0, 0, ((MessagesContract.ChannelSelectedListener) getActivity())));
        if (channelDataList != null && channelDataList.size() > 0)
            ((MessagesContract.ChannelSelectedListener) getActivity()).onChannelSelected(channelDataList.get(0).getServerId(), channelDataList.get(0).getName());
    }

    @Override
    public void loadChannelsError(String errorMessage) {
        Log.i("", "error " + errorMessage);
    }
}
