package byteis.com.bytecomm.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import byteis.com.bytecomm.ui.navigator.Navigator;
import byteis.com.bytecomm.ui.navigator.NavigatorImp;

public class BaseActivity extends AppCompatActivity {

    protected Navigator navigator;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigator = NavigatorImp.getInstance();
    }

    protected void setError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
        textInputLayout.getEditText().setError(error);
    }

    protected void showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate) {
        if (!isFinishing()) {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(this);

            if (!progressDialog.isShowing()) {
                progressDialog.setMessage(dialogMessage);
                progressDialog.setCancelable(isCancelable);
                progressDialog.setIndeterminate(isIndeteminate);
                progressDialog.show();
            }
        }
    }

    protected void dismissProgressDialog() {
        if (!isFinishing()) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

}
