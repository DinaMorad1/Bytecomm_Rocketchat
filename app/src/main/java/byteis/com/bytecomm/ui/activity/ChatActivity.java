package byteis.com.bytecomm.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.databinding.ActivityChatBinding;
import byteis.com.bytecomm.ui.contact.MessagesContract;
import byteis.com.bytecomm.ui.fragment.MessagesFragment;

public class ChatActivity extends BaseActivity implements MessagesContract.ChannelSelectedListener {

    private ActivityChatBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
    }

    @Override
    public void onChannelSelected(String roomServerId, String roomTitle) {
        MessagesFragment messagesFragment = (MessagesFragment) getFragmentManager().findFragmentById(R.id.fragment_messaging);
        messagesFragment.onChannelSelected(roomServerId, roomTitle);
        messagesFragment.setUp(binding.drawerLayout);
        closeDrawer();
    }

    private void closeDrawer() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
            binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout != null && binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
