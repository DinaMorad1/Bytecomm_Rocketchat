package byteis.com.bytecomm.ui.navigator;

import android.content.Context;
import android.content.Intent;

import byteis.com.bytecomm.ui.activity.ChatActivity;
import byteis.com.bytecomm.ui.activity.LoginActivity;

public class NavigatorImp implements Navigator {

    private static NavigatorImp instance;

    public static NavigatorImp getInstance() {
        if (instance == null)
            instance = new NavigatorImp();
        return instance;
    }

    private NavigatorImp() {
    }

    @Override
    public void navigateChatActivity(Context context) {
        Intent chatIntent = new Intent(context, ChatActivity.class);
        context.startActivity(chatIntent);
    }

    @Override
    public void navigateLoginActivity(Context context) {
        Intent loginIntent = new Intent(context, LoginActivity.class);
        context.startActivity(loginIntent);
    }
}
