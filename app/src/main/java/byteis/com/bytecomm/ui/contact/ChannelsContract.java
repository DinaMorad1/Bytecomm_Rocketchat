package byteis.com.bytecomm.ui.contact;

import java.util.List;

import byteis.com.bytecomm.api.model.ChannelData;

public interface ChannelsContract {

    interface ChannelsView {

        void loadChannels(List<ChannelData> channelDataList);

        void loadChannelsError(String errorMessage);
    }


    interface ServerChannelsListener {
        void onGetChannelsSuccess(List<ChannelData> channels);

        void onError(String errorMessage);
    }
}
