package byteis.com.bytecomm.ui.helpers;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;

public class GlideHelper {
    public static void loadUrl(Context context, String url, ImageView imageView) {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .crossFade()
                .fitCenter()
                .centerCrop()
                .into(imageView);
    }

    public static void loadUrl(Context context, String url, SimpleTarget target) {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .crossFade()
                .fitCenter()
                .centerCrop()
                .into(target);
    }

    public static void loadFile(Context context, File file, ImageView imageView) {
        Glide.with(context)
                .load(file)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .crossFade()
                .into(imageView);
    }

    public static void loadResource(Context context, int drawable, ImageView imageView) {
        Glide.with(context)
                .load("")
                .placeholder(drawable)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .crossFade()
                .fitCenter()
                .centerCrop()
                .into(imageView);
    }
}
