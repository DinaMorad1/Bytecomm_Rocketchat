package byteis.com.bytecomm.ui.holders;

import android.support.annotation.NonNull;
import android.view.View;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;

import byteis.com.bytecomm.api.model.ChannelData;
import byteis.com.bytecomm.databinding.LayoutChannelBinding;
import byteis.com.bytecomm.model.Header;

public class ChannelViewHolder extends ChildViewHolder {

    public LayoutChannelBinding binding;

    public ChannelViewHolder(LayoutChannelBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(@NonNull ChannelData channelData){
        binding.itemTitle.setText(channelData.getName());
        binding.statusImgView.setVisibility(View.GONE);
    }


}
