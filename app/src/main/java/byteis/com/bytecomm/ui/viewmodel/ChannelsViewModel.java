package byteis.com.bytecomm.ui.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;

import java.util.List;

import byteis.com.bytecomm.api.RetrofitHelper;
import byteis.com.bytecomm.api.model.ChannelData;
import byteis.com.bytecomm.ui.contact.ChannelsContract;

public class ChannelsViewModel extends BaseObservable implements ChannelsContract.ServerChannelsListener {

    private Context mContext;
    private ChannelsContract.ChannelsView channelsView;

    public ChannelsViewModel(Context mContext, ChannelsContract.ChannelsView channelsView){
        this.mContext = mContext;
        this.channelsView = channelsView;
        fetchChannels();
    }

    private void fetchChannels() {
        RetrofitHelper.getInstance(mContext).getChannels(this);
    }


    @Override
    public void onGetChannelsSuccess(List<ChannelData> channels) {
        channelsView.loadChannels(channels);
    }

    @Override
    public void onError(String errorMessage) {
        channelsView.loadChannelsError(errorMessage);
    }
}
