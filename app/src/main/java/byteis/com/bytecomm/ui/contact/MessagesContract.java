package byteis.com.bytecomm.ui.contact;

import java.util.List;

import byteis.com.bytecomm.api.model.MessageData;

public interface MessagesContract {

    interface MessagesView extends ServerMessagesListener, ChannelSelectedListener {

    }

    interface ChannelSelectedListener {
        void onChannelSelected(String roomServerId, String roomTitle);
    }

    interface ServerMessagesListener {
        void onGetMessagesSuccess(List<MessageData> messageDataList);

        void onError(String errorMessage);
    }
}
