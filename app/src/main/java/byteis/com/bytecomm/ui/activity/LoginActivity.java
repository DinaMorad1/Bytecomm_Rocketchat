package byteis.com.bytecomm.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.model.UserData;
import byteis.com.bytecomm.databinding.ActivityLoginBinding;
import byteis.com.bytecomm.ui.contact.LoginViewModelContract;
import byteis.com.bytecomm.ui.helpers.SnackbarHelper;
import byteis.com.bytecomm.ui.viewmodel.LoginViewModel;

public class LoginActivity extends BaseActivity implements LoginViewModelContract.LoginView {

    private ActivityLoginBinding activityLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setLoginvm(new LoginViewModel(this, this));
    }

    @Override
    public void setError(String emailError, String passwordError) {
        if (!emailError.isEmpty())
            setError(activityLoginBinding.emailTextInput, emailError);

        if (!passwordError.isEmpty())
            setError(activityLoginBinding.passwordTextInput, passwordError);

        SnackbarHelper.showSnackbar(getString(R.string.fill_fields), activityLoginBinding.getRoot(), this);
    }

    @Override
    public void clearErrors() {
        setError(activityLoginBinding.emailTextInput, null);
        setError(activityLoginBinding.passwordTextInput, null);
    }

    @Override
    public void showProgress() {
        showProgressDialog(getString(R.string.please_wait), false, true);
    }

    @Override
    public void hideProgress() {
        dismissProgressDialog();
    }

    @Override
    public void loginSuccess(UserData userData) {
        finish();
        navigator.navigateChatActivity(this);
    }

    @Override
    public void loginError(String errorStr) {
        SnackbarHelper.showSnackbar(errorStr, activityLoginBinding.getRoot(), this);
    }
}
