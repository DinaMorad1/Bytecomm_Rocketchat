package byteis.com.bytecomm.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.data.SharedPref;

public class SplashActivity extends BaseActivity {

    public final static int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        initHandler();
    }

    private void initHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPref.checkKey(SplashActivity.this, SharedPref.AUTH_TOKEN) && SharedPref.checkKey(SplashActivity.this, SharedPref.USER_ID))
                    navigator.navigateChatActivity(SplashActivity.this);
                else
                    navigator.navigateLoginActivity(SplashActivity.this);

                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
