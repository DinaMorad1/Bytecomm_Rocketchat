package byteis.com.bytecomm.ui.contact;

import byteis.com.bytecomm.api.model.Response;
import byteis.com.bytecomm.api.model.UserData;

public interface LoginViewModelContract {

    interface LoginView extends ServerLoginListener<UserData> {

        void setError(String email, String password);

        void clearErrors();

        void showProgress();

        void hideProgress();

    }

    interface ServerLoginListener<T> {
        void loginSuccess(T data);

        void loginError(String errorStr);
    }

//    interface ViewModel {
//        void destroy();
//    }

}
