package byteis.com.bytecomm.ui.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;

import byteis.com.bytecomm.R;
import byteis.com.bytecomm.api.RestConstants;
import byteis.com.bytecomm.api.RetrofitHelper;
import byteis.com.bytecomm.api.model.User;
import byteis.com.bytecomm.api.model.UserData;
import byteis.com.bytecomm.data.SharedPref;
import byteis.com.bytecomm.ui.contact.LoginViewModelContract;

public class LoginViewModel extends BaseObservable implements LoginViewModelContract.ServerLoginListener<UserData> {

    private User user;
    private Context mContext;
    private LoginViewModelContract.LoginView loginView;

    public LoginViewModel(Context mContext, LoginViewModelContract.LoginView loginView) {
        this.user = new User();
        this.mContext = mContext;
        this.loginView = loginView;
    }

    @Bindable
    public String getEmail() {
        return user.getEmail();
    }

    public void setEmail(String email) {
        user.setEmail(email);
        notifyPropertyChanged(BR.email);
    }


    @Bindable
    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
        notifyPropertyChanged(BR.password);
    }

    public View.OnClickListener onLoginClicked() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginView.clearErrors();
                if (getEmail() == null || getPassword() == null || getEmail().isEmpty() || getPassword().isEmpty()) {
                    String emailError = (getEmail() == null || getEmail().isEmpty()) ? mContext.getString(R.string.empty_email) : "";
                    String passwordError = (getPassword() == null || getPassword().isEmpty()) ? mContext.getString(R.string.empty_password) : "";
                    loginView.setError(emailError, passwordError);
                } else {
                    loginView.showProgress();
                    RetrofitHelper.getInstance(mContext).userLogin(getEmail(), getPassword(), LoginViewModel.this);
                }

            }
        };
    }

    @Override
    public void loginSuccess(UserData userData) {
        SharedPref.saveString(mContext, SharedPref.AUTH_TOKEN, userData.getAuthToken());
        SharedPref.saveString(mContext, SharedPref.USER_ID, userData.getUserId());
        loginView.hideProgress();
        loginView.loginSuccess(userData);
    }

    @Override
    public void loginError(String errorStr) {
        loginView.hideProgress();
        loginView.loginError(errorStr);
    }
}
