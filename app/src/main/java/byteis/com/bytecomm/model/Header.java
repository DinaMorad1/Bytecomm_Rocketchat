package byteis.com.bytecomm.model;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

import byteis.com.bytecomm.api.model.ChannelData;

public class Header implements Parent<ChannelData> {

    private List<ChannelData> channelDataList;

    private String headerText;

    public Header(String headerText, List<ChannelData> channelDataList){
        this.headerText = headerText;
        this.channelDataList = channelDataList;
    }

    @Override
    public List<ChannelData> getChildList() {
        return channelDataList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }

    public String getHeaderText() {
        return headerText;
    }

    @Override
    public boolean equals(Object obj) {
        boolean sameSame = false;
        if (obj != null && obj instanceof Header) {
            sameSame = this.headerText == ((Header) obj).getHeaderText();
        }
        return sameSame;
    }
}
